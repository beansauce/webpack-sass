const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
//const PurifyCSSPlugin = require('purifycss-webpack');
const path = require('path');
const bootstrapEntryPoints = require('./webpack.bootstrap.config');
const autoprefixer = require('autoprefixer');

//var glob = require('glob');


var isProd = process.env.NODE_ENV === 'production'; // true or false
var cssDev = ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'];
var cssProd = ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: ['css-loader', 'postcss-loader', 'sass-loader'],
    publicPath: '/dist/styles/'
});

var cssConfig = isProd ? cssProd : cssDev;
var bootstrapConfig = isProd ? bootstrapEntryPoints.prod : bootstrapEntryPoints.dev;

module.exports = {
    entry: {
        app: './src/scripts/app.js',
        bootstrap: bootstrapConfig
    },
// TODO: Something wrong w/ the output on prod, creates internal dist folder when it shouldn't
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: '[name].bundle.js'
    },

    module: {
        rules: [
            {
                test: /\.scss$/,
                use: cssConfig
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: 'url-loader?name=fonts/[name].[ext]?limit=10000'
            },
            {
                test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
                use: 'file-loader?name=fonts/[name].[ext]'
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                exclude: /(node_modules)/,
                use: [
                    //if you want to use a hash image name of 6 chars
                    //'file-loader?name=images/[hash:6].[ext]' // if you want to use hash image name limited to 6 chars

                    //'file-loader?name=[name].[ext]&outputPath=images/&publicPath=images/',  //allows you to specify different output and public paths
                    'file-loader?name=images/[name].[ext]', //uses the images directory for both the output and public paths
                    {
                        loader: 'image-webpack-loader',
                        query: {
                            mozjpeg: {
                                progressive: true
                            },
                            gifsicle: {
                                interlaced: false
                            },
                            optipng: {
                                optimizationLevel: 4
                            },
                            pngquant: {
                                quality: '75-90',
                                speed: 3
                            }
                        }
                    }
                ]
            },
            {
                test: /bootstrap-sass\/assets\/javascripts\//,
                use: 'imports-loader?jQuery=jquery'
            },

        ]
    },
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        hot: true,
        //port: 9000,  // can be 8080 or 9000
        //tats: 'errors-only',
        open: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Getting better',

            // for minifying html
            //			minify: {
            //				collapseWhitespace: true
            //			},

            // updates js name with random chars to show it changes on every build.
            hash: true,
            excludeChunks: ['contact'],
            template: './src/index.ejs' // Load a custom template (ejs by default see the FAQ for details)
        }),
        new HtmlWebpackPlugin({
            title: 'Contact Page',
            filename: 'contact.html',
            hash: true,
            excludeChunks: ['app'],
            template: './src/contact.html'
        }),

        new ExtractTextPlugin({
            filename: 'styles/[name].css',
            disable: !isProd,
            allChunks: true
        }),
        /*        new PurifyCSSPlugin({
         // Give paths to parse for rules. These should be absolute!
         paths: glob.sync(path.join(__dirname, '*.html')),
         }),*/
        new webpack.HotModuleReplacementPlugin(),

        new webpack.NamedModulesPlugin(),


        new webpack.LoaderOptionsPlugin({
            postcss: [autoprefixer]
        })

    ]
};
